# Lost Pets App
* This is a very simple app build with Spring Boot that allows you to login and
fill a report if you lost your cat or dog.

### Libraries/Tools used:
- Front-End - Bootstrap
- Back-End - Spring Boot/Spring MVC
- Database - MYSQL(`root/database-schema/schema.sql`)


#### Some Screenshots 
![scr](https://i.ibb.co/cYk84Ky/Screenshot-from-2019-09-29-10-50-01.png)

![scr](https://i.ibb.co/s1xdx8w/Screenshot-from-2019-09-29-10-37-53.png)

![scr](https://i.ibb.co/W5KdxLH/Screenshot-from-2019-09-29-10-37-12.png)

![scr](https://i.ibb.co/thHMdvb/Screenshot-from-2019-09-29-10-37-10.png)

![scr](https://i.ibb.co/T062Mk9/Screenshot-from-2019-09-29-10-34-08.png)

![scr](https://i.ibb.co/g37tnxb/Screenshot-from-2019-09-29-10-33-59.png)

![scr](https://i.ibb.co/r6vct36/Screenshot-from-2019-09-29-10-33-58.png)

![scr](https://i.ibb.co/4YZmpsc/Screenshot-from-2019-09-29-10-33-49.png)

![scr](https://i.ibb.co/kQQRMKj/Screenshot-from-2019-09-29-10-33-44.png)

![scr](https://i.ibb.co/D8JTFph/Screenshot-from-2019-09-29-10-33-33.png)
