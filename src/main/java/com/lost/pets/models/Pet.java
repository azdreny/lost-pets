/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lost.pets.models;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author azdren
 */
@Entity
@Table(name = "pets_table")
public class Pet {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    
     
    @JoinColumn(name = "userid")
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private User user;
    
    @NotBlank(message  = "You need to provide a name")
    private String name;
    
    @NotBlank(message = "Please provide image url of our lost pet")
    private String imageUrl;
    
    private String info;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "You need to provide the date you lost your pet")
    private Date lostDate;
    
    @NotBlank(message = "You need to provide your cell phone number")
    @Pattern(regexp = "^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{3}$", 
            message= "Provide a valid 049/044-123-123 number")
    private String contactNumber;

    @Lob
    @Column(length=512)
    private String note;
    
    @NotBlank(message = "Address can\'t be empty")
    private String address;
    
    @Column(columnDefinition= "tinyint(1) default 1")
    private boolean status;

    public Pet() {
    }
    

    public Pet(String name, String imageUrl, String info, Date lostDate, String contactNumber, String note, String address, boolean status) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.info = info;
        this.lostDate = lostDate;
        this.contactNumber = contactNumber;
        this.note = note;
        this.address = address;
        this.status = status;
    }

    public Pet(User user, String name, String imageUrl, String info, Date lostDate, String contactNumber, String note, String address, boolean status) {
        this.user = user;
        this.name = name;
        this.imageUrl = imageUrl;
        this.info = info;
        this.lostDate = lostDate;
        this.contactNumber = contactNumber;
        this.note = note;
        this.address = address;
        this.status = status;
    }
    
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Date getLostDate() {
        return lostDate;
    }

    public void setLostDate(Date lostDate) {
        this.lostDate = lostDate;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Pet{" + "id=" + id + ", name=" + name + ", imageUrl=" + imageUrl + ", info=" + info + ", lostDate=" + lostDate + ", contactNumber=" + contactNumber + ", note=" + note + ", address=" + address + ", status=" + status + '}';
    }
    
}
