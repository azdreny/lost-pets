/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lost.pets;

import com.lost.pets.models.Pet;
import com.lost.pets.models.Role;
import com.lost.pets.models.User;
import com.lost.pets.repositories.PetRepository;
import com.lost.pets.repositories.RoleRepository;
import com.lost.pets.repositories.UserRepository;
import java.util.Date;
import java.util.Arrays;
import java.util.HashSet;

import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author azdren
 */
@Component
public class DatabaseSeeder {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PetRepository petRepository;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public DatabaseSeeder(PetRepository petRepository,
            UserRepository userRepository,
            BCryptPasswordEncoder passwordEncoder,
            RoleRepository roleRepository) {
        this.petRepository = petRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        cleanDb();
        seedData();
    }

    void cleanDb() {
        userRepository.deleteAll();
        petRepository.deleteAll();
        roleRepository.deleteAll();
    }

    void seedData() {
        roleRepository.save(new Role("USER"));

        Date date = new Date(System.currentTimeMillis());
        
        User user = new User("John","Doe","john@gmail.com",passwordEncoder.encode("123456"));
        User user2 = new User("Janed","Doe","janed@gmail.com",passwordEncoder.encode("123456"));
        User user3 = new User("Azdren","Ymeri","azdreny@gmail.com",passwordEncoder.encode("123456"));
        
        Pet pet1 = new Pet(user,
                "Max", // name
                "https://live.staticflickr.com/7019/6493880695_4297b97154_b.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "44 High Crossing Park", // address
                true);  // status lost true or false 

        Pet pet2 = new Pet(user,
                "Buster", // name
                "https://farm8.staticflickr.com/7008/26541974030_e3a2fea2cb_b.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "96099 Fordem Lane", // address
                true);  // status lost true or false 
        Pet pet3 = new Pet(user,
                "Duke", // name
                "https://upload.wikimedia.org/wikipedia/commons/0/0e/Duke-Spanish_Mastiff.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "6876 Dayton Street", // address
                true);  // status lost true or false 
        Pet pet4 = new Pet(user2,
                "Cooper", // name
                "https://upload.wikimedia.org/wikipedia/commons/a/a4/Racib%C3%B3rz_2007_082.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "01700 Stoughton Lane", // address
                true);  // status lost true or false 
        Pet pet5 = new Pet(user2,
                "Riley", // name
                "https://live.staticflickr.com/4112/5170590074_714d36db83_b.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "606 Waubesa Plaza", // address
                false);  // status lost true or false 
        Pet pet6 = new Pet(user2,
                "Tucker", // name
                "https://upload.wikimedia.org/wikipedia/commons/8/86/Maltese_puppy.jpeg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "1 Summer Ridge Terrace", // address
                true);  // status lost true or false 
        Pet pet7 = new Pet(user3,
                "Zoe", // name
                "https://cdn.pixabay.com/photo/2019/03/23/05/15/schafer-dog-4074699_960_720.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "764 Weeping Birch Alley", // address
                false);  // status lost true or false 
        Pet pet8 = new Pet(user3,
                "Abby", // name
                "https://upload.wikimedia.org/wikipedia/commons/b/b1/American_Pitbull_Terrier_-_Colby_Line.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "1212 Corscot Center", // address
                true);  // status lost true or false 
        Pet pet9 = new Pet(user3,
                "Coco", // name
                "https://upload.wikimedia.org/wikipedia/commons/0/0e/01_Wire_Fox_terrier.jpg", // image
                "Info", // Info
                date, // lost date
                "044-123-123", // contact number
                "This is a very cool dog", // note
                "76 Butterfield Alley", // address
                false);  // satus lost true or false         "044-123-123", // contact number

        user.setPets(new HashSet<>(Arrays.asList(pet1, pet2, pet3)));
        user2.setPets(new HashSet<>(Arrays.asList(pet4, pet5, pet6)));
        user3.setPets(new HashSet<>(Arrays.asList(pet7, pet8, pet9)));
       
       
        userRepository.save(user);
        userRepository.save(user2);
        userRepository.save(user3);
        
        user.setRoles(new HashSet<>(Arrays.asList(roleRepository.findByRole("USER"))));
        user2.setRoles(new HashSet<>(Arrays.asList(roleRepository.findByRole("USER"))));
        user3.setRoles(new HashSet<>(Arrays.asList(roleRepository.findByRole("USER"))));
        
        
        userRepository.save(user);
        userRepository.save(user2);
        userRepository.save(user3);

    }

}
