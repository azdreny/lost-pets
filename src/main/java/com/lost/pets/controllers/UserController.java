/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lost.pets.controllers;

import com.lost.pets.models.User;
import com.lost.pets.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author azdren
 */
@Controller
public class UserController {
    
    private final UserRepository userRepository;
    
    @Autowired
    public UserController(UserRepository userRepository){
        this.userRepository = userRepository;
    }
        
    @GetMapping("/users")
    public String getAllUsers(Model model){
         Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth.getName().equals("anonymousUser")){
         return "redirect:/login";   
        }
        User user = userRepository.findByEmail(auth.getName());
        
        model.addAttribute("users",userRepository.findAll());
        model.addAttribute("currentUser", user);
        
        return "users";
    }
}
