    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lost.pets.controllers;
import com.lost.pets.models.User;
import com.lost.pets.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author azdren
 */

@Controller
public class HomeController {

    @Autowired
    UserRepository userRepository;
    
    @GetMapping("/")
    public String index(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!auth.getName().equals("anonymousUser")){
         return "redirect:/home";   
        }
        return "index";
    }
   
    @GetMapping("/home")
    public String home(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());
         if(user != null){
             model.addAttribute("myReportedPets",user.getPets());
             return "home";
         }
        return "index";
    }
    
    @ModelAttribute("currentUser")
    public User getCurrentUser() {
 
      Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
      User user = userRepository.findByEmail(auth.getName());
       return user;
    }
}
