/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lost.pets.controllers;

import com.lost.pets.models.Role;
import com.lost.pets.models.User;
import com.lost.pets.repositories.RoleRepository;
import com.lost.pets.repositories.UserRepository;
import java.util.Arrays;
import java.util.HashSet;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author azdren
 */
@Controller
public class LoginController {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
   @GetMapping("/login")
    public String login(User user){
          Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!auth.getName().equals("anonymousUser")){
         return "redirect:/home";   
        }
        return "login-form";
    }
    
    @PostMapping("/login")
    public String authUser(@Valid User user,BindingResult result,Model model){
        
        if(result.hasErrors()){
            System.out.println("User Has errors");
            return "login-form";
        }
        
        User usr = userRepository.findByEmail(user.getEmail());
        System.out.println(usr.toString());
        if(usr != null){
            model.addAttribute("currentUser",usr);
            System.out.println("Login success");
            return "redirect:/home";
        }
        System.out.println("re-rendering the login-form");
        return "login-form";
    }
    
    @GetMapping("/signup")
    public String signup(User user){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!auth.getName().equals("anonymousUser")){
         return "redirect:/home";   
        }
        
        return "signup-form";
    }
    
    @PostMapping("/signup")
    public String create(@Valid User user,BindingResult result,Model model){
        if(result.hasErrors()){
            return "signup-form";
        }

        String encPass = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encPass);
        user.setActive(1);
        Role userRole = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        
        if(userRepository.save(user) != null){
            model.addAttribute("currentUser",user);
            return "/home";
        }
        System.out.println("SAVED::: "+user.toString());
        return "signup-form";
    }
}
