/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lost.pets.controllers;

import com.lost.pets.models.Pet;
import com.lost.pets.models.User;
import com.lost.pets.repositories.PetRepository;
import com.lost.pets.repositories.UserRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author azdren
 */
@Controller
public class PetController {

    private PetRepository petRepository;
    private UserRepository userRepository;

    @Autowired
    public PetController(PetRepository petRepository, UserRepository userRepository) {
        this.petRepository = petRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/pets")
    public String getAllPets(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getName().equals("anonymousUser")) {
            return "redirect:/login";
        }
        User user = userRepository.findByEmail(auth.getName());
        model.addAttribute("pets", petRepository.findAll());
        return "pets";
    }

    @GetMapping("/pets/new")
    public String showPetForm(Pet pet) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getName().equals("anonymousUser")) {
            return "redirect:/login";
        }
        return "pet-form";
    }

    @PostMapping("/pets/create")
    public String createPet(@Valid Pet pet, BindingResult result) {
        if (result.hasErrors()) {
            return "pet-form";
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = userRepository.findByEmail(auth.getName());
        if (user != null) {

            pet.setUser(user);
            petRepository.save(pet);
            return "redirect:/pets";
        }
        return "redirect:/login";
    }

    @GetMapping("/pets/{id}/details")
    public String petDetails(@PathVariable("id") long id, Model model) {
        Pet pet = petRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Wrond id"));
        model.addAttribute("pet", pet);
        return "pet-details";
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser() {
 
      Authentication auth =  SecurityContextHolder.getContext().getAuthentication();
      User user = userRepository.findByEmail(auth.getName());
       return user;
    }
}
