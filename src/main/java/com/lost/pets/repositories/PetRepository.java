/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lost.pets.repositories;

import com.lost.pets.models.Pet;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author azdren
 */
public interface PetRepository extends CrudRepository<Pet, Long> {
  
}
